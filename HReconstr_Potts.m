function HReconstr_Potts

% Copyright:	Andrew L. Ferguson, UIUC 
% Last updated:	11 Mar 2016

% SYNOPSIS
%
% code to translate multiple sequence alignment (MSA) of viral protein strains into Potts spin glass model of fitness landscape by semi-analytic Newton descent 

% INPUT FILES
%
% MSA.mat               - multiple sequence alignment nSeq sequences each comprising m amino acids denoted by single letter codes 
% h_in.dat              - (optional) initial values of Potts model h parameters  
% J_in.dat              - (optional) initial values of Potts model J parameters  

% OUTPUT FILES
%
% MSA_numeric.mat       - translation of MSA from single letter amino acid codes to integer codes 
% resIdx.dat            - list of all amino acid residue integer codes observed in MSA at each position in decreasing order of prevalence 
% P1_target.dat         - one-position amino acid frequencies for each observed residue at each position computed from MSA 
% P2_target.dat         - two-position amino acid frequencies for each observed residue at each pair of positions computed from MSA 
% h_traj.dat            - trajectory of Potts model h parameters over the course of model fitting 
% J_traj.dat            - trajectory of Potts model J parameters over the course of model fitting 
% P1_traj.dat           - trajectory of one-position amino acid frequencies predicted by the model over the course of model fitting evaluated by MC sampling 
% P2_traj.dat           - trajectory of two-position amino acid frequencies predicted by the model over the course of model fitting evaluated by MC sampling 
% P1_targReg_traj.dat   - trajectory of regularized one-position amino acid target frequencies over the course of model fitting 
% P2_targReg_traj.dat   - trajectory of regularized two-position amino acid target frequencies over the course of model fitting 
% h_final.dat           - final values of Potts model h parameters  
% J_final.dat           - final values of Potts model J parameters  


tic;

%% 0. inputs
seed=200184;                        % rng seed
beta=1;                             % inverse temperature
nIter=500;                          % # Newton iterations 
nStep=1000;                         % # MC steps per Newton iteration
regType=4;                          % regularization type (1=L1, 2=L2, 3=pseudo-counts, 4=3+1, 5=3+2)
lambda_h=0.1;                       % regularization parameter for L1 & L2 h reg (for 1=L1 adjust P1_target by sign(h)*1/nSeq*lambda_h, for 2=L2 adjust P1_target by 2*h*1/nSeq*lambda_h ) 
lambda_J=0.1;                       % regularization parameter for L1 & L2 J reg (for 1=L1 adjust P2_target by sign(J)*1/nSeq*lambda_J, for 2=L2 adjust P2_target by 2*J*1/nSeq*lambda_J ) 
lambda_P1=0.0;                      % regularization parameter for P1 psuedo-counts (for 3=pseudo-counts adjust P1_target{i} by lambda_P1/nRes(i) )
lambda_P2=0.1;                      % regularization parameter for P2 psuedo-counts (for 3=pseudo-counts adjust P2_target{i,j} by lambda_P2/[nRes(i)*nRes(j)] )

gaugeFixh=1;						% gauge fix h_i{i}(1)=0
gaugeFixJ=1;						% gauge fix J_ij{i,j}(:,1)=J_ij{i,j}(1,:)=0

tol_MARG=1E-3;                      % used for regType=3,4,5 (those involving pseudo-counts), the target probabilities are regularized with pseudo-counts once at the start of the run, unlike the adjustable regType=1,2 (L1,L2) h & J regularization, we iteratively converge the regularized P1 and P2 targets to assure marginal consistency and present a viable convergence goal 
cntr_max_MARG=1000;                 % maximum number of iterations in P2 marginal renormalization for consistency with P1 under regularization 

gamma_h=0.5;                        % softening parameter on h steps
gamma_J=0.5;                        % softening parameter on J steps

maxNewtonStep_h=0.1;                % maximum permissible absolute value of h_i Newton steps, higher values are thresholded
maxNewtonStep_J=0.1;                % maximum permissible absolute value of J_ij Newton steps, higher values are thresholded

write_mod=10;                       % iteration modulus for file writes 
print_mod=10;                       % iteration modulus for screen reports

loadhJ=0;                           % load initial h and J values from h_in.dat and J_in.dat 


%% 1. probabilities

% importing and sizing the MSA
load('MSA.mat','MSA');
MSA = aa2int(MSA);
save('MSA_numeric.mat','MSA');
nSeq = size(MSA,1);                         % # sequences
m = size(MSA,2);                            % # positions
uniqueResIdx = unique(MSA(:));              % unique residue indices residing in MSA


% one-position residue ordering and target probabilities
hist1=histc(MSA,uniqueResIdx);              % one-position mutational histograms
[hist1,order1]=sort(hist1,1,'descend');     % reordering the residue indices at each position in descending order of prevalence 

resIdx=cell(1,m);
nRes=nan(1,m);
P1_target=cell(1,m);
for i=1:m
	resIdx{i}=uniqueResIdx(order1([hist1(:,i)>0],i));
	nRes(i)=length(resIdx{i});
	P1_target{i}=hist1([hist1(:,i)>0],i)/nSeq;
end


% two-position target probabilities
% \-> matrix indexing based on the one-position residue ordering in each pair 
P2_target=cell(m,m);
for i=1:m
	for j=i+1:m
		[edge_i,idx_sort_i]=sort(resIdx{i},'ascend');            % hist3 requires that edges must be monotonically increasing 
		[edge_j,idx_sort_j]=sort(resIdx{j},'ascend');

		idx_unsort_i=nan(length(idx_sort_i),1);
		idx_unsort_j=nan(length(idx_sort_j),1);
		idx_unsort_i(idx_sort_i)=1:1:length(idx_sort_i);
		idx_unsort_j(idx_sort_j)=1:1:length(idx_sort_j);

		p=hist3(MSA(:,[i,j]),'Edges',{edge_i,edge_j});
		p=p(idx_unsort_i,idx_unsort_j)/nSeq;                     % reordering and normalizing
		P2_target{i,j}=p;
	end
end


% writing resIdx, P1_target and P2_target to file
	
fout_resIdx = fopen('resIdx.dat','wt');
for i=1:m
	fprintf(fout_resIdx,'%6d\t\t',i);
	fprintf(fout_resIdx,'%6d\t',resIdx{i});
	fprintf(fout_resIdx,'\n');
end
fprintf(fout_resIdx,'\n');
fclose(fout_resIdx);

fout_P1_target = fopen('P1_target.dat','wt');
for i=1:m
	fprintf(fout_P1_target,'%15.5e',P1_target{i});
end
fprintf(fout_P1_target,'\n');
fclose(fout_P1_target);

fout_P2_target = fopen('P2_target.dat','wt');
for i=1:m
	for j=i+1:m
		fprintf(fout_P2_target,'%15.5e',P2_target{i,j});
	end
end
fprintf(fout_P2_target,'\n');
fclose(fout_P2_target);



%% 2. Potts initialization 
%  -> we gauge fix to zero:	the most common residue h value in each position (h_i{i}(1), by construction)  
%                     		the row and column in the J matrix associated with the most common residues at each position (J_ij{i,j}(1,:) and J_ij{i,j}(:,1), by construction) 

if loadhJ
    
    % h
    fin_h = fopen('h_in.dat','rt');
    	line = fgetl(fin_h);            % reading line
		line = sscanf(line,'%f');   	% splitting line
		n_el = length(line);           	% sizing
    fclose(fin_h);
    if n_el~=sum(nRes)
    	error('# elements in loaded h file does not correspond to expected number of entries');
    end
    
    h_i=cell(1,m);
    idx=1;
    for i=1:m
        for p=1:nRes(i)
        	h_i{i}(p,1)=line(idx);
        	idx=idx+1;
        end
        if gaugeFixh==1
            h_i{i} = h_i{i} - repmat(h_i{i}(1),[size(h_i{i}),1]);		% gauge fixing
        end
    end
    
    % J
    fin_J = fopen('J_in.dat','rt');
    	line = fgetl(fin_J);            % reading line
		line = sscanf(line,'%f');   	% splitting line
		n_el = length(line);           	% sizing
    fclose(fin_J);
    if n_el~=sum(sum(triu(nRes'*nRes,1)))
    	error('# elements in loaded J file does not correspond to expected number of entries');
    end
    
    J_ij=cell(m,m);
    idx=1;
	for i=1:m
        for j=i+1:m
            for q=1:nRes(j)
                for p=1:nRes(i)						% column major ordering
					J_ij{i,j}(p,q)=line(idx);
					idx=idx+1;
                end
            end
            if gaugeFixJ==1
                J_ij{i,j} = J_ij{i,j} - repmat(J_ij{i,j}(1,:),[size(J_ij{i,j},1),1]);		% gauge fixing
                J_ij{i,j} = J_ij{i,j} - repmat(J_ij{i,j}(:,1),[1,size(J_ij{i,j},2)]);
            end
        end
	end
    
else

    h_i=cell(1,m);
    for i=1:m
    	h_i{i}=zeros(nRes(i),1);
        for p=1:nRes(i)
        	h_i{i}(p) = -log(P1_target{i}(p))/beta;
        end
        h_i{i} = h_i{i} - repmat(h_i{i}(1),[size(h_i{i}),1]);                             % initial gauge fixing of independent position approximation estimates
    end
    
    J_ij=cell(m,m);
    for i=1:m
        for j=i+1:m
            J_ij{i,j}=zeros(nRes(i),nRes(j));
            J_ij{i,j} = J_ij{i,j} - repmat(J_ij{i,j}(1,:),[size(J_ij{i,j},1),1]);
            J_ij{i,j} = J_ij{i,j} - repmat(J_ij{i,j}(:,1),[1,size(J_ij{i,j},2)]);
        end
    end

end


%% 3. semi-analytical Newton descent

% initializing z0 (baseline state from which MC sampling commences in each round) with most probable residue in each position 
z0=zeros(1,m);
for i=1:m
    z0(i)=resIdx{i}(1);
end


% initializing regularized and model probability arrays
P1_target_reg = P1_target;
P2_target_reg = P2_target;
P1_model = P1_target;
P2_model = P2_target;
for i=1:m
    P1_target_reg{i} = P1_target_reg{i}*NaN;
    P1_model{i} = P1_model{i}*0;
    for j=i+1:m
        P2_target_reg{i,j} = P2_target_reg{i,j}*NaN;
        P2_model{i,j} = P2_model{i,j}*0;
    end
end
if ( regType==4 || regType==5 )
    P1_target_adj = P1_target;
    P2_target_adj = P2_target;
    for i=1:m
        P1_target_adj{i} = P1_target_adj{i}*NaN;
        for j=i+1:m
            P2_target_adj{i,j} = P2_target_adj{i,j}*NaN;
        end
    end
end


% initializing rng
rand('twister',seed);


% initializing output files
fout_h = fopen('h_traj.dat','wt');
fout_J = fopen('J_traj.dat','wt');
fout_P1 = fopen('P1_traj.dat','wt');
fout_P2 = fopen('P2_traj.dat','wt');
fout_P1t = fopen('P1_targReg_traj.dat','wt');
fout_P2t = fopen('P2_targReg_traj.dat','wt');


% main loop
fprintf('Commencing semi-analytical Newton iterations...\n');
for iter=1:nIter
    
    % 1. establishing (regularized) target probabilities 
    % \-> regTypes 1 (L1) and 2 (L2) depend on values of h & J, so must be updated on each pass
    %     regType 3 (pseudo-counts) does not, and need be initialized only once at iter==0 
    %     regTypes 4 (3+1) and 5 (3+2) use a mixture of pseudo-counts and L1 or L2, so must also be updated on each pass 
    if (regType==1 || regType==2 || (regType==3 && iter==1) || regType==4 || regType==5 )
    
        for i=1:m
            if regType==1
                P1_target_reg_TMP = P1_target{i} + (lambda_h/nSeq)*sign(h_i{i});                        % regularization
            elseif regType==2
                P1_target_reg_TMP = P1_target{i} + (lambda_h/nSeq)*2*h_i{i};                            % regularization
            elseif ( regType==3 || (regType==4 && iter==1) || (regType==5 && iter==1) )
                P1_target_reg_TMP = P1_target{i} + (lambda_P1/nRes(i))*ones(nRes(i),1);
                
                P1_target_reg_TMP(P1_target_reg_TMP<0)=0;
                if any(P1_target_reg_TMP(:)<0)
                    error('Regularization has induced negative elements in P1_target_reg(%d)',i);
                end
                
                P1_target_reg_TMP = P1_target_reg_TMP / sum(P1_target_reg_TMP);                         % renormalization st sum_i P1_target_reg{i} = 1, retaining elements in relative proportion
                
                if ( regType==4 || regType==5 )
                    P1_target_adj{i} = P1_target_reg_TMP;
                end
            elseif (regType==4 && iter~=1)
                P1_target_reg_TMP = P1_target_adj{i} + (lambda_h/nSeq)*sign(h_i{i});                    % regularization
            elseif (regType==5 && iter~=1)
                P1_target_reg_TMP = P1_target_adj{i} + (lambda_h/nSeq)*2*h_i{i};                        % regularization
            else
                error('regType unrecognized');
            end
            P1_target_reg{i} = P1_target_reg_TMP;
        end
        
        for i=1:m
            for j=i+1:m
                if regType==1
                    P2_target_reg_TMP = P2_target{i,j} + (lambda_J/nSeq)*sign(J_ij{i,j});                   % regularization
                elseif regType==2
                    P2_target_reg_TMP = P2_target{i,j} + (lambda_J/nSeq)*2*J_ij{i,j};                       % regularization
                elseif ( regType==3 || (regType==4 && iter==1) || (regType==5 && iter==1) )
                    P2_target_reg_TMP = P2_target{i,j} + (lambda_P2/(nRes(i)*nRes(j)))*ones(nRes(i),nRes(j));
                    
                    P2_target_reg_TMP(P2_target_reg_TMP<0)=0;
                    if any(P2_target_reg_TMP(:)<0)
                        error('Regularization has induced negative elements in P2_target_reg(%d,%d)',i,j);
                    end

                    err_MARG=Inf;                                                                           % marginals renormalization
                    cntr_MARG=0;

                    P1_r = P1_target_reg{i};
                    P1_c = P1_target_reg{j}';

                    while err_MARG > tol_MARG
                        r=sum(P2_target_reg_TMP,2);
                        err_r=sum(abs(r-P1_r));
                        P2_target_reg_TMP=diag(P1_r./r)*P2_target_reg_TMP;
                        c=sum(P2_target_reg_TMP,1);
                        err_c=sum(abs(c-P1_c));
                        P2_target_reg_TMP=P2_target_reg_TMP*diag(P1_c./c);
                        err_MARG = (err_r + err_c)/m;
                        cntr_MARG = cntr_MARG + 1;
                        if cntr_MARG > cntr_max_MARG
                            error('Cannot converge P2 marginals to P1 values for P_target_reg(%d,%d)',i,j);
                        end
                    end
                    
                    if ( regType==4 || regType==5 )
                        P2_target_adj{i,j} = P2_target_reg_TMP;
                    end
                elseif (regType==4 && iter~=1)
                    P2_target_reg_TMP = P2_target_adj{i,j} + (lambda_J/nSeq)*sign(J_ij{i,j});                   % regularization
                elseif (regType==5 && iter~=1)
                    P2_target_reg_TMP = P2_target_adj{i,j} + (lambda_J/nSeq)*2*J_ij{i,j};                       % regularization
                else
                    error('regType unrecognized');
                end
                P2_target_reg{i,j} = P2_target_reg_TMP;
            end
        end
        
    end
    
    
    % 2. MC evaluation of model probabilities
    z=z0;
    for i=1:m
        P1_model{i} = P1_model{i}*0;
        for j=i+1:m
            P2_model{i,j} = P2_model{i,j}*0;
        end
    end
    for step=1:nStep
        
        % Metropolis
        ran1=rand(1);
        ran2=rand(1);
        ran3=rand(1);
        
        position_MC = ceil(ran1*m);
        
        res_MC_old = z(position_MC);
        res_MC_new = resIdx{position_MC}(ceil(ran2*nRes(position_MC)));
        
        delta_E=0;
        for j=1:m
            if j==position_MC
                delta_E = delta_E + h_i{position_MC}(resIdx{position_MC}==res_MC_new) - h_i{position_MC}(resIdx{position_MC}==res_MC_old);
            else
                if position_MC < j
                    delta_E = delta_E + J_ij{position_MC,j}(resIdx{position_MC}==res_MC_new,resIdx{j}==z(j)) - J_ij{position_MC,j}(resIdx{position_MC}==res_MC_old,resIdx{j}==z(j));
                else
                    delta_E = delta_E + J_ij{j,position_MC}(resIdx{j}==z(j),resIdx{position_MC}==res_MC_new) - J_ij{j,position_MC}(resIdx{j}==z(j),resIdx{position_MC}==res_MC_old);
                end
            end
        end
        
        if ran3 <= exp(-beta*delta_E)
            z(position_MC) = res_MC_new;
        end
        
        % histogramming
        for i=1:m
            P1_model{i}(resIdx{i}==z(i)) =  P1_model{i}(resIdx{i}==z(i)) + 1;
            for j=i+1:m
                P2_model{i,j}(resIdx{i}==z(i),resIdx{j}==z(j)) =  P2_model{i,j}(resIdx{i}==z(i),resIdx{j}==z(j)) + 1;
            end
        end
        
    end
    
    
    % histo -> prob
    for i=1:m
        P1_model{i} =  P1_model{i} / nStep;
        for j=i+1:m
            P2_model{i,j} =  P2_model{i,j} / nStep;
        end
    end
    
    
    % 3. reporting
    if mod(iter,print_mod)==0
    	fprintf('  Completed iteration %10d of %10d\n',iter,nIter);
    end
    
    if mod(iter,write_mod)==0
    	
    	fprintf(fout_h,'%15d\t',iter);
        for i=1:m
			fprintf(fout_h,'%15.5e',h_i{i});
        end
		fprintf(fout_h,'\n');
		
		fprintf(fout_J,'%15d\t',iter);
		for i=1:m
			for j=i+1:m
				fprintf(fout_J,'%15.5e',J_ij{i,j});
			end
		end
		fprintf(fout_J,'\n');
		
		fprintf(fout_P1,'%15d\t',iter);
		for i=1:m
			fprintf(fout_P1,'%15.5e',P1_model{i});
		end
		fprintf(fout_P1,'\n');
		
		fprintf(fout_P2,'%15d\t',iter);
		for i=1:m
			for j=i+1:m
				fprintf(fout_P2,'%15.5e',P2_model{i,j});
			end
		end
		fprintf(fout_P2,'\n');
		
		fprintf(fout_P1t,'%15d\t',iter);
		for i=1:m
			fprintf(fout_P1t,'%15.5e',P1_target_reg{i});
		end
		fprintf(fout_P1t,'\n');
		
		fprintf(fout_P2t,'%15d\t',iter);
		for i=1:m
			for j=i+1:m
				fprintf(fout_P2t,'%15.5e',P2_target_reg{i,j});
			end
		end
		fprintf(fout_P2t,'\n');
		
    end
    
    
    % 4. Newton stepping in h and J
   	%  -> we gauge fix to zero:	the most common residue h value in each position (h_i{i}(1), by construction)  
	%                     		the row and column in the J matrix associated with the most common residues at each position (J_ij{i,j}(1,:) and J_ij{i,j}(:,1), by construction) 
    %  -> in this block we either: compute the Newton stepped h & J for the next iteration, or dump terminal h and J values to file 
	if iter==nIter
    	
    	fout_h_final = fopen('h_final.dat','wt');
		for i=1:m
			fprintf(fout_h_final,'%15.5e',h_i{i});
		end
		fprintf(fout_h_final,'\n');
		fclose(fout_h_final);
		
		fout_J_final = fopen('J_final.dat','wt');
		for i=1:m
			for j=i+1:m
				fprintf(fout_J_final,'%15.5e',J_ij{i,j});
			end
		end
		fprintf(fout_J_final,'\n');
    	fclose(fout_J_final);
    	
    	break;
    	
    else
    
        for i=1:m
			P_t = P1_target_reg{i};
			P_m = P1_model{i};
			diff = P_t-P_m;
			step = diff./(P_m.*(P_m-ones(size(P_m))));
			step(isnan(step)) = sign(diff(isnan(step)))*maxNewtonStep_h;
			step(abs(step)>maxNewtonStep_h) = sign(step(abs(step)>maxNewtonStep_h))*maxNewtonStep_h;
			h_i{i} = h_i{i} + gamma_h*step;
            if gaugeFixh==1
                h_i{i} = h_i{i} - repmat(h_i{i}(1),[size(h_i{i}),1]);		% gauge fixing
            end
        end
        for i=1:m
            for j=i+1:m
				P_t = P2_target_reg{i,j};
				P_m = P2_model{i,j};
				diff = P_t-P_m;
				step = diff./(P_m.*(P_m-ones(size(P_m))));
				step(isnan(step)) = sign(diff(isnan(step)))*maxNewtonStep_J;
				step(abs(step)>maxNewtonStep_J) = sign(step(abs(step)>maxNewtonStep_J))*maxNewtonStep_J;
				J_ij{i,j} = J_ij{i,j} + gamma_J*step;
                if gaugeFixJ==1
                    J_ij{i,j} = J_ij{i,j} - repmat(J_ij{i,j}(1,:),[size(J_ij{i,j},1),1]);		% gauge fixing
                    J_ij{i,j} = J_ij{i,j} - repmat(J_ij{i,j}(:,1),[1,size(J_ij{i,j},2)]);
                end
            end
        end

	end

    
end
fprintf('DONE!\n\n');


% closing output trajectory files
fclose(fout_h);
fclose(fout_J);
fclose(fout_P1);
fclose(fout_P2);
fclose(fout_P1t);
fclose(fout_P2t);

toc;

return

