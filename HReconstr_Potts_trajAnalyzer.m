function HReconstr_Potts_trajAnalyzer

% Copyright:	Andrew L. Ferguson, UIUC 
% Last updated:	11 Mar 2016

% SYNOPSIS
%
% code to plot trajectory files written by HReconstr_Potts.m to assess model convergence 

% INPUT FILES
%
% h_traj.dat            - trajectory of Potts model h parameters over the course of model fitting 
% J_traj.dat            - trajectory of Potts model J parameters over the course of model fitting 
% P1_traj.dat           - trajectory of one-position amino acid frequencies predicted by the model over the course of model fitting evaluated by MC sampling 
% P2_traj.dat           - trajectory of two-position amino acid frequencies predicted by the model over the course of model fitting evaluated by MC sampling 
% P1_targReg_traj.dat   - trajectory of regularized one-position amino acid target frequencies over the course of model fitting 
% P2_targReg_traj.dat   - trajectory of regularized two-position amino acid target frequencies over the course of model fitting 
% P1_target.dat         - one-position amino acid frequencies for each observed residue at each position computed from MSA 
% P2_target.dat         - two-position amino acid frequencies for each observed residue at each pair of positions computed from MSA 


% OUTPUT FILES
%
% h_traj.fig/jpg            - trajectory of Potts model h parameters over the course of model fitting 
% J_traj.fig/jpg            - trajectory of Potts model J parameters over the course of model fitting 
% P1_traj.fig/jpg           - trajectory of one-position amino acid frequencies predicted by the model over the course of model fitting evaluated by MC sampling 
% P2_traj.fig/jpg           - trajectory of two-position amino acid frequencies predicted by the model over the course of model fitting evaluated by MC sampling 
% P1_targReg_traj.fig/jpg   - trajectory of regularized one-position amino acid target frequencies over the course of model fitting 
% P2_targReg_traj.fig/jpg   - trajectory of regularized two-position amino acid target frequencies over the course of model fitting 
% P1_fit.fig/jpg            - parity plot of one-position amino acid target frequencies computed from the MSA vs. one-position amino acid frequencies predicted by the Potts model averaged over the fitting trajectory 
% P2_fit.fig/jpg            - parity plot of two-position amino acid target frequencies computed from the MSA vs. two-position amino acid frequencies predicted by the Potts model averaged over the fitting trajectory 



%% parameters
fsize=12;


% sizing screen for figure generation
set(0,'Units','pixels') 
scnsize = get(0,'ScreenSize');
pos_M = [scnsize(3)/3, scnsize(4)/3, scnsize(3)/3, scnsize(4)/3];
pos_N = [scnsize(3)/3, 2*scnsize(4)/3, scnsize(3)/3, scnsize(4)/3];
pos_S = [scnsize(3)/3, 5, scnsize(3)/3, scnsize(4)/3];
pos_E = [2*scnsize(3)/3, scnsize(4)/3, scnsize(3)/3, scnsize(4)/3];
pos_W = [5, scnsize(4)/3, scnsize(3)/3, scnsize(4)/3];





traj_file='./h_traj.dat';

% running file once to size array
fid = fopen(traj_file,'rt');

n_snap = 0;
while feof(fid) == 0

	line = fgetl(fid);              % reading line
	if n_snap == 0
		line = sscanf(line,'%f');   % splitting line
		line = line(2:end);         % rejecting timestamp
		n_el = length(line);           % sizing
	end

	n_snap = n_snap + 1;

end

fclose(fid);

% reading array
t = zeros(n_snap,1);
traj_array = zeros(n_snap,n_el);

fid = fopen(traj_file,'rt');

for snap=1:n_snap

	line = sscanf(fgetl(fid),'%f');
	t(snap) = line(1);
	traj_array(snap,:) = line(2:end)';

end

fclose(fid);

% plotting trajectories
fig_handle = figure('Position',pos_W);

plot(t,traj_array);
xlabel('iteration ','fontsize',fsize);
ylabel('value ','fontsize',fsize);
set(gca,'fontsize',fsize);

saveas(gcf,traj_file(1:end-4),'fig');
print(gcf,'-djpeg',[traj_file(1:end-4),'.jpg']);

close(fig_handle);





traj_file='./J_traj.dat';

% running file once to size array
fid = fopen(traj_file,'rt');

n_snap = 0;
while feof(fid) == 0

	line = fgetl(fid);              % reading line
	if n_snap == 0
		line = sscanf(line,'%f');   % splitting line
		line = line(2:end);         % rejecting timestamp
		n_el = length(line);           % sizing
	end

	n_snap = n_snap + 1;

end

fclose(fid);

% reading array
t = zeros(n_snap,1);
traj_array = zeros(n_snap,n_el);

fid = fopen(traj_file,'rt');

for snap=1:n_snap

	line = sscanf(fgetl(fid),'%f');
	t(snap) = line(1);
	traj_array(snap,:) = line(2:end)';

end

fclose(fid);

% plotting trajectories
fig_handle = figure('Position',pos_E);

plot(t,traj_array);
xlabel('iteration ','fontsize',fsize);
ylabel('value ','fontsize',fsize);
set(gca,'fontsize',fsize);

saveas(gcf,traj_file(1:end-4),'fig');
print(gcf,'-djpeg',[traj_file(1:end-4),'.jpg']);

close(fig_handle);





traj_file='./P1_traj.dat';

% running file once to size array
fid = fopen(traj_file,'rt');

n_snap = 0;
while feof(fid) == 0

	line = fgetl(fid);              % reading line
	if n_snap == 0
		line = sscanf(line,'%f');   % splitting line
		line = line(2:end);         % rejecting timestamp
		n_el = length(line);           % sizing
	end

	n_snap = n_snap + 1;

end

fclose(fid);

% reading array
t = zeros(n_snap,1);
traj_array = zeros(n_snap,n_el);

fid = fopen(traj_file,'rt');

for snap=1:n_snap

	line = sscanf(fgetl(fid),'%f');
	t(snap) = line(1);
	traj_array(snap,:) = line(2:end)';

end

fclose(fid);

% plotting trajectories
fig_handle = figure('Position',pos_N);

plot(t,traj_array);
xlabel('iteration ','fontsize',fsize);
ylabel('value ','fontsize',fsize);
set(gca,'fontsize',fsize);

saveas(gcf,traj_file(1:end-4),'fig');
print(gcf,'-djpeg',[traj_file(1:end-4),'.jpg']);

close(fig_handle);

P1_mean = mean(traj_array,1);
P1_std = std(traj_array,0,1);




traj_file='./P2_traj.dat';

% running file once to size array
fid = fopen(traj_file,'rt');

n_snap = 0;
while feof(fid) == 0

	line = fgetl(fid);              % reading line
	if n_snap == 0
		line = sscanf(line,'%f');   % splitting line
		line = line(2:end);         % rejecting timestamp
		n_el = length(line);           % sizing
	end

	n_snap = n_snap + 1;

end

fclose(fid);

% reading array
t = zeros(n_snap,1);
traj_array = zeros(n_snap,n_el);

fid = fopen(traj_file,'rt');

for snap=1:n_snap

	line = sscanf(fgetl(fid),'%f');
	t(snap) = line(1);
	traj_array(snap,:) = line(2:end)';

end

fclose(fid);

% plotting trajectories
fig_handle = figure('Position',pos_S);

plot(t,traj_array);
xlabel('iteration ','fontsize',fsize);
ylabel('value ','fontsize',fsize);
set(gca,'fontsize',fsize);

saveas(gcf,traj_file(1:end-4),'fig');
print(gcf,'-djpeg',[traj_file(1:end-4),'.jpg']);

close(fig_handle);

P2_mean = mean(traj_array,1);
P2_std = std(traj_array,0,1);




traj_file='./P1_targReg_traj.dat';

% running file once to size array
fid = fopen(traj_file,'rt');

n_snap = 0;
while feof(fid) == 0

	line = fgetl(fid);              % reading line
	if n_snap == 0
		line = sscanf(line,'%f');   % splitting line
		line = line(2:end);         % rejecting timestamp
		n_el = length(line);           % sizing
	end

	n_snap = n_snap + 1;

end

fclose(fid);

% reading array
t = zeros(n_snap,1);
traj_array = zeros(n_snap,n_el);

fid = fopen(traj_file,'rt');

for snap=1:n_snap

	line = sscanf(fgetl(fid),'%f');
	t(snap) = line(1);
	traj_array(snap,:) = line(2:end)';

end

fclose(fid);

% plotting trajectories
fig_handle = figure('Position',pos_N);

plot(t,traj_array);
xlabel('iteration ','fontsize',fsize);
ylabel('value ','fontsize',fsize);
set(gca,'fontsize',fsize);

saveas(gcf,traj_file(1:end-4),'fig');
print(gcf,'-djpeg',[traj_file(1:end-4),'.jpg']);

close(fig_handle);

P1_targetReg_mean = mean(traj_array,1);
P1_targetReg_std = std(traj_array,0,1);




traj_file='./P2_targReg_traj.dat';

% running file once to size array
fid = fopen(traj_file,'rt');

n_snap = 0;
while feof(fid) == 0

	line = fgetl(fid);              % reading line
	if n_snap == 0
		line = sscanf(line,'%f');   % splitting line
		line = line(2:end);         % rejecting timestamp
		n_el = length(line);           % sizing
	end

	n_snap = n_snap + 1;

end

fclose(fid);

% reading array
t = zeros(n_snap,1);
traj_array = zeros(n_snap,n_el);

fid = fopen(traj_file,'rt');

for snap=1:n_snap

	line = sscanf(fgetl(fid),'%f');
	t(snap) = line(1);
	traj_array(snap,:) = line(2:end)';

end

fclose(fid);

% plotting trajectories
fig_handle = figure('Position',pos_S);

plot(t,traj_array);
xlabel('iteration ','fontsize',fsize);
ylabel('value ','fontsize',fsize);
set(gca,'fontsize',fsize);

saveas(gcf,traj_file(1:end-4),'fig');
print(gcf,'-djpeg',[traj_file(1:end-4),'.jpg']);

close(fig_handle);
P2_targetReg_mean = mean(traj_array,1);
P2_targetReg_std = std(traj_array,0,1);




% P1_fit
traj_file='./P1_target.dat';
fid = fopen(traj_file,'rt');
    P1_target = fgetl(fid);                 % reading line
    P1_target = sscanf(P1_target,'%f');     % splitting line
fclose(fid);


fig_handle = figure('Position',pos_N);

scatter(P1_target,P1_mean,25,'r');
hold on
plot([0 1],[0 1],'-k');
hold off
xlabel('P1\_target ','fontsize',fsize);
ylabel('P1\_model (mean) ','fontsize',fsize);
set(gca,'fontsize',fsize); 
xlim([0 1.1*max([P1_target',P1_mean])])
ylim([0 1.1*max([P1_target',P1_mean])])

saveas(gcf,'P1_fit','fig');
print(gcf,'-djpeg','P1_fit.jpg');

close(fig_handle);





% P2_fit
traj_file='./P2_target.dat';
fid = fopen(traj_file,'rt');
    P2_target = fgetl(fid);                 % reading line
    P2_target = sscanf(P2_target,'%f');     % splitting line
fclose(fid);


fig_handle = figure('Position',pos_S);

scatter(P2_target,P2_mean,25,'r');
hold on
plot([0 1],[0 1],'-k');
hold off
xlabel('P2\_target ','fontsize',fsize);
ylabel('P2\_model (mean) ','fontsize',fsize);
set(gca,'fontsize',fsize); 
xlim([0 1.1*max([P2_target',P2_mean])])
ylim([0 1.1*max([P2_target',P2_mean])])

saveas(gcf,'P2_fit','fig');
print(gcf,'-djpeg','P2_fit.jpg');

close(fig_handle);